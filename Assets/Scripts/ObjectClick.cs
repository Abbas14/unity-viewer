﻿using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;


public class ObjectClick : MonoBehaviour
{
    public GameObject hotspot_Canvas;
    public GameObject Load;
    public GameObject panel;
    public Text data;
    public string APIUrl = "https://snapdplfunction.azurewebsites.net/api/GetQueryRecord?code=QbNhZUi03YCmJXoZTaXZ9DBMRUN4sDwakGSHVd3zcQJ2wJlgH/G8ZQ==&username=mathankumar2601@gmail.com&packname=sqlpacktest.DPLPack&queryname=applicationcopy";

    public Text Value
    {
        get { return data; }
    }

    public void Cor_Answer()
    {
        hotspot_Canvas.SetActive(false);
    }

    public void OnMouseDown()
    {
        
        StartCoroutine(getjsonvalue());
        hotspot_Canvas.SetActive(true);
        
    }


    IEnumerator getjsonvalue()
    {
        string url = APIUrl;
        using (UnityWebRequest request = new UnityWebRequest(url, UnityWebRequest.kHttpVerbGET))
        { 
            request.downloadHandler = new DownloadHandlerBuffer();
            request.SetRequestHeader("Content-Type", "application/json");
            yield return request.SendWebRequest();

            if (request.isNetworkError || request.isHttpError)
            {
                Debug.Log(request.error);
            }
            else if (request.responseCode == 200)
            {
                string Data = FixJson(request.downloadHandler.text);
                var creature = JsonConvert.DeserializeObject<RootObject>(Data);
                Value.text = creature.ToString();
                Value.text = "";
            }
        }
    }


    string FixJson(string value)
    {
        value = "{\"Items\":" + value + "}" ;
        return value;
    }

    public class Items
    {
            public int APP_ID { get; set; }
            public string APPLICATION_NAME { get; set; }
            public string APPLICATION_VERSION { get; set; }
            public object APPLICATION_SERVICE_PACK { get; set; }
            public string APPLICATION_PATCH_LEVEL { get; set; }
    }

   
    public class RootObject
    {
        public List<Items> Items { get; set; }
    }
}
