﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.UI;

public class GuiId_Genrate : MonoBehaviour
{
    public InputField Value ;
   
    public void get()
    {
        Value.text = GetUniqueID();
    }
    public static string GetUniqueID()
    {
        string key = "ID";
        var random = new System.Random();
        DateTime epochStart = new System.DateTime(1970, 1, 1, 8, 0, 0, System.DateTimeKind.Utc);
        double timestamp = (System.DateTime.UtcNow - epochStart).TotalSeconds;

       string   uniqueID = String.Format("{0:X}", Convert.ToInt32(timestamp))                
                + "-" + String.Format("{0:X}", Convert.ToInt32(Time.time * 1000000))        
                + "-" + String.Format("{0:X}", random.Next(1000000000));                

        Debug.Log("Generated Unique ID: " + uniqueID);
        return uniqueID;
    }
}
