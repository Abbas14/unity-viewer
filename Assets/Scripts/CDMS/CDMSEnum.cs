﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assets.Scripts.CDMS
{
    public enum PresentationType
    {
        AlertMessage,
        Chart,
        Grid
    }

    public enum MessageType
    {
        Warring,
        CautionAlert

    }

   public enum ChartType
    {
        LineChart
    }

}
