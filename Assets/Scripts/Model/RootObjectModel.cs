﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assets.Scripts.Model
{
    public class RootObjectModel
    {
        public string NodeId { get; set; }
        public string PresentationType { get; set; }
        public string MessageType { get; set; }
        public string ResponseField { get; set; }
        public List<AzureFunction> AzureFunction { get; set; }
        public string ChartType { get; set; }

    }
    public class Param
    {
        public string username { get; set; }
        public string packname { get; set; }
        public string queryname { get; set; }
    }

    public class AzureFunction
    {
        public string AuthType { get; set; }
        public string RequestURL { get; set; }
        public string FunctionKey { get; set; }
        public string FunctionName { get; set; }
        public List<Param> Params { get; set; }
    }
}
