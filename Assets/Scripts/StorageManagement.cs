﻿using ICSharpCode.SharpZipLib.Zip;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Security;
using System.Security.Cryptography.X509Certificates;
using System.Text.RegularExpressions;
using TriLib;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using Assets.Scripts.Model;
using System.Security.Permissions;
public class StorageManagement : MonoBehaviour
{
    public Button DropBox;




    void Start()
    {

        DropBox.onClick.AddListener(() => { DropLdFile(); });



    }

    public void Gizmos()
    {
        ApplicationContext.ISGizmo = !ApplicationContext.ISGizmo;
    }
    public void DropLdFile()
    {
        string dropboxGetFileJsion = "{\"path\": \"//\"}";
        var getfiles = GetWebRequest("POST", dropboxGetFileJsion);
        var drplist = JsonUtility.FromJson<DropBoxItems>(getfiles);
        AddText(drplist);
    }
    public void LocalStorage()
    {
        string[] filepath = Directory.GetFiles(ApplicationContext.persDataPath + "/");
        Debug.Log(filepath);
    }
    void AddText(DropBoxItems dropBoxItems)

    {
        foreach (var item in dropBoxItems.entries)
        {
            objectName.text = item.name;
            GameObject dorpboxobj = Instantiate(listMethod);
            dorpboxobj.transform.SetParent(transform, false);
            dorpboxobj.AddComponent(typeof(EventTrigger));
            EventTrigger trigger = dorpboxobj.GetComponent<EventTrigger>();
            EventTrigger.Entry entry = new EventTrigger.Entry
            {
                eventID = EventTriggerType.PointerClick
            };
            entry.callback.AddListener((eventData) =>
            {
                LoadFile(eventData);
            });
            trigger.triggers.Add(entry);
        }
    }
    public string seletedObject { get; set; }

    public Text objectName;
    public GameObject listMethod;
    private List<StorageManagement> _dropbox = new List<StorageManagement>();
    public List<StorageManagement> DropBoxList
    {
        get { return _dropbox; }
    }
    public string PackageName { get; set; }

    public void Bindingtobutton(string PackageName)
    {
        PackageName = objectName.text;
        int index = DropBoxList.FindIndex(x => x.PackageName == objectName.text);
        if (index == -1)
        {
            GameObject dorpboxobj = Instantiate(listMethod);
            dorpboxobj.transform.SetParent(transform, false);
        }
    }
    public void BindLabelObjectEvent(string roomName)
    {
        roomName = objectName.text;
    }
    public void LoadFile(BaseEventData baseEvent)
    {
        PointerEventData pointerEventData = (PointerEventData)baseEvent;
        GameObject gameObject = pointerEventData.pointerCurrentRaycast.gameObject;
        var text = gameObject.GetComponent<Text>().text;
        var files = DownloadDropBoxFiles(text);

        foreach (var item in files)
        {
            if (!item.ToLower().Contains("json"))
            {
                LoadGameObject(item);
            }
            else
            {
                CovertModelJson(item);
            }
        }

    }
    public List<string> DownloadDropBoxFiles(string path)
    {
        try
        {
            string tragetFolder = path;
            List<string> files = new List<string>();
            string responseBody = string.Empty;
            var httpWebRequest = (HttpWebRequest)WebRequest.Create(ApplicationContext.APIDropBoxDowanldURI);
            httpWebRequest.Method = "POST";
            httpWebRequest.Headers.Add("Authorization: Bearer " + ApplicationContext.Token);
            httpWebRequest.Headers.Add("Dropbox-API-Arg", "{\"path\": \"" + ("/" + path) + "\"}");
            int bytesProcessed = 0;
            ServicePointManager.ServerCertificateValidationCallback = MyRemoteCertificateValidationCallback;
            var response = httpWebRequest.GetResponse();
            if (response != null)
            {
                ServicePointManager.ServerCertificateValidationCallback = MyRemoteCertificateValidationCallback;
                var remoteStream = response.GetResponseStream();
                var localStream = File.Create(ApplicationContext.persDataPath + path);
                byte[] buffer = new byte[1024];
                int bytesRead;
                do
                {
                    bytesRead = remoteStream.Read(buffer, 0, buffer.Length);
                    localStream.Write(buffer, 0, bytesRead);
                    bytesProcessed += bytesRead;
                }
                while (bytesRead > 0);
                localStream.Close();
                remoteStream.Close();
            }
            int fileExtPos = path.LastIndexOf(".");
            if (fileExtPos >= 0)
                tragetFolder = path.Substring(0, fileExtPos);
            if (!Directory.Exists(ApplicationContext.persDataPath + "/" + tragetFolder))
            {
                Directory.CreateDirectory(ApplicationContext.persDataPath + "/" + tragetFolder);
            }

            ZipFile zipFile = new ZipFile(ApplicationContext.persDataPath + "/" + path);
            foreach (ZipEntry entry in zipFile)
            {
                string targetFile = Path.Combine(ApplicationContext.persDataPath + "/" + tragetFolder, entry.Name);

                using (FileStream outputFile = File.Create(targetFile))
                {
                    if (entry.Size > 0)
                    {
                        Stream zippedStream = zipFile.GetInputStream(entry);
                        byte[] buffer = new byte[1024];
                        int bytesRead;
                        do
                        {
                            bytesRead = zippedStream.Read(buffer, 0, buffer.Length);
                            outputFile.Write(buffer, 0, bytesRead);
                            bytesProcessed += bytesRead;
                        } while (bytesRead > 0);
                        outputFile.Close();
                    }
                }
                files.Add(targetFile);

            }
            zipFile.Close();
            File.Delete(ApplicationContext.persDataPath + "/" + path);
            return files;
        }
        catch (Exception ex)
        {
            Debug.Log(ex.Message);
            return null;
        }
    }
    private void ObjectExcuteData(BaseEventData baseEvent)
    {
        PointerEventData pointerEventData = (PointerEventData)baseEvent;
        GameObject gameObject = pointerEventData.pointerCurrentRaycast.gameObject;
    }
    private string GetWebRequest(string requestMethod, string requestJson)
    {
        try
        {
            string responseBody = string.Empty;
            var httpWebRequest = (System.Net.HttpWebRequest)WebRequest.Create(ApplicationContext.APIDropBoxGetFileURI);
            httpWebRequest.Method = requestMethod;
            httpWebRequest.Headers.Add("Authorization: Bearer " + ApplicationContext.Token);
            httpWebRequest.ContentType = "application/json";
            ServicePointManager.ServerCertificateValidationCallback = MyRemoteCertificateValidationCallback;
            using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
            {
                string json = requestJson;
                streamWriter.Write(json);
            }
            ServicePointManager.ServerCertificateValidationCallback = MyRemoteCertificateValidationCallback;
            var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
            using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
            {
                var result = streamReader.ReadToEnd();
                responseBody = result;
            }
            return responseBody;
        }
        catch (Exception ex)
        {
            Debug.Log(ex.Message);
            return null;
        }
    }
    public void LoadGameObject(string path)
    {
        using (var assetLoader = new AssetLoader())
        {
            try
            {
                var assetLoaderOptions = AssetLoaderOptions.CreateInstance();
                assetLoaderOptions.RotationAngles = new Vector3(90, 0, 0);
                assetLoaderOptions.AutoPlayAnimations = true;
                var objes = assetLoader.LoadFromFile(path, assetLoaderOptions);
            }
            catch (Exception ex)
            {
                Debug.Log(ex.Message);
            }
        }

    }
    public bool MyRemoteCertificateValidationCallback(System.Object sender, X509Certificate certificate, X509Chain chain, SslPolicyErrors sslPolicyErrors)
    {
        bool isOk = true;
        if (sslPolicyErrors != SslPolicyErrors.None)
        {
            for (int i = 0; i < chain.ChainStatus.Length; i++)
            {
                if (chain.ChainStatus[i].Status != X509ChainStatusFlags.RevocationStatusUnknown)
                {
                    chain.ChainPolicy.RevocationFlag = X509RevocationFlag.EntireChain;
                    chain.ChainPolicy.RevocationMode = X509RevocationMode.Online;
                    chain.ChainPolicy.UrlRetrievalTimeout = new TimeSpan(0, 1, 0);
                    chain.ChainPolicy.VerificationFlags = X509VerificationFlags.AllFlags;
                    bool chainIsValid = chain.Build((X509Certificate2)certificate);
                    if (!chainIsValid)
                    {
                        isOk = false;
                    }
                }
            }
        }
        return isOk;
    }

    private void CovertModelJson(string path)
    {
        var data = File.ReadAllText(path);
        var rootObjectModels = JsonConvert.DeserializeObject<List<RootObjectModel>>(data);
        if (ApplicationContext.PackageRootObjectModel == null)
        {
            ApplicationContext.PackageRootObjectModel = new List<RootObjectModel>();
        }
        ApplicationContext.PackageRootObjectModel.AddRange(rootObjectModels);

    }
}
