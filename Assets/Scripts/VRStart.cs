﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class VRStart : MonoBehaviour {
    public GameObject VR_gameobject,Desktopgameobject,FPSgameobject;
	// Use this for initialization
	void Start () {
        //VR_gameobject.SetActive(true);
        Desktopgameobject.SetActive(true);
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    public void VREnable()
    {
        VR_gameobject.SetActive(true);
        Desktopgameobject.SetActive(false);
        FPSgameobject.SetActive(false);
    }

    public void DesktopEnable()
    {
        VR_gameobject.SetActive(false);
        Desktopgameobject.SetActive(true);
        FPSgameobject.SetActive(false);
    }

    public void FPSEnable()
    {
        VR_gameobject.SetActive(false);
        Desktopgameobject.SetActive(false);
        FPSgameobject.SetActive(true);
    }


}
