﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PDF_Popup : MonoBehaviour
{
    public bool enableText;

   public GameObject PDF_Viewer;
   public GameObject PDF_Rendererbtn;
   public GameObject Page, inputField;

    public void PDF_viewer()
    {
        PDF_Viewer.SetActive(true);
    }

    public void PDF_Viewer_Disable()
    {
        PDF_Viewer.SetActive(false);
    }

    public void PDF_btn_show()
    {
        PDF_Rendererbtn.SetActive(true);
    }

    public void PDF_btn_hide()
    {
        PDF_Rendererbtn.SetActive(false);
    }

    public void page()
    {
        
    }
}
