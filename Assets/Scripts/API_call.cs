﻿using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Text.RegularExpressions;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class API_call : MonoBehaviour
{

    public GameObject LoginPage;
    public GameObject panel;
    public GameObject Panel;
    public StreamReader reader;
    public UnityWebRequest webRequest;
    public InputField UserNameField;
    public InputField PasswordField;
    public Dropdown m_Dropdown;
    public Dropdown Usertype_Dropdown;
    List<string> m_DropOptions = new List<string>();

    [Serializable]
    public class domain1
    {
        [JsonProperty(PropertyName = "_id")]
        public string id { get; set; }

        [JsonProperty(PropertyName = "domain")]
        public string domain { get; set; }
    }

    void Start()
    {
        UserNameField.onEndEdit.AddListener(delegate { LockInput(UserNameField); });
    }

    IEnumerator Get(string input)
    {
        UnityWebRequest request = UnityWebRequest.Get("https://workspapi.azurewebsites.net/api/getDomain/" + input);
        yield return request.SendWebRequest();

        if (request.isNetworkError || request.isHttpError)
        {
            Debug.Log(request.error);
        }
        else
        {
            Debug.Log(request.downloadHandler.text);
            if (request.downloadHandler.text != "")
            {
                var data = request.downloadHandler.text.Split(',');
                for (int i = 0; i < data.Length; i++)
                {

                    if (data[i].Contains("id"))
                    {
                        var arr = data[i].Split(':');
                        var id = Regex.Replace(arr[1], @"[^0-9a-zA-Z]+", ",").Replace(",", string.Empty);
                        m_Dropdown.itemText.text = id;
                    }
                    if (data[i].Contains("domain"))
                    {
                        var arr = data[i].Split(':');
                        var domainname = Regex.Replace(arr[1], @"[^0-9a-zA-Z]+", ",").Replace(",", string.Empty);
                        m_DropOptions.Add(domainname);
                    }
                }
                m_Dropdown.AddOptions(m_DropOptions);

            }
            byte[] results = request.downloadHandler.data;
        }
    }

    void LockInput(InputField input)
    {
        if (Regex.IsMatch(input.text, @"\A(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?)\Z", RegexOptions.IgnoreCase))
        {
            StartCoroutine(Get(input.text));
            Panel.gameObject.SetActive(false);
        }
        else
            Panel.gameObject.SetActive(true);
    }

    IEnumerator XapiPostLogin(string json)
    {
        using (UnityWebRequest request = new UnityWebRequest("http://ec2-54-226-111-193.compute-1.amazonaws.com/data/xAPI/statements", UnityWebRequest.kHttpVerbPOST))
        {
            byte[] bodyRaw = System.Text.Encoding.UTF8.GetBytes(json);

            request.uploadHandler = (UploadHandler)new UploadHandlerRaw(bodyRaw);
            request.downloadHandler = (DownloadHandler)new DownloadHandlerBuffer();
            request.SetRequestHeader("Authorization", "Basic M2M1ZGRkNDRmN2FiZDZlNzI2NmYxNzk4YjNlNTNjOGE5MDBjZDdiNTo3ZTcxNDgzYmRmMWIxMzFlNDcxN2VjOWZkMDU0ZjA4Nzg2ODM2MTAx");
            request.SetRequestHeader("X-Experience-API-Version", "1.0.3");
            request.SetRequestHeader("Content-Type", "application/json");

            yield return request.SendWebRequest();

            if (request.isNetworkError || request.isHttpError)
            {
                Debug.Log(request.error);

            }
            else
            {

            }
        }
    }
    public void LoadXapiModel(LoginModel mod)
    {
        XapiModel.Actor act = new XapiModel.Actor()
        {
            mbox = "mailto:" + mod.email
        };
        XapiModel.Display dis = new XapiModel.Display() { Domain = mod.domainId, Type = mod.type, Username = mod.email };

        XapiModel.Verb verb = new XapiModel.Verb()
        {
            id = "https://www.google.com/",
            display = dis
        };
        XapiModel.Object obj = new XapiModel.Object() { id = "https://www.google.com/" };
        XapiModel.DataModel data = new XapiModel.DataModel()
        {
            actor = act,
            verb = verb,
            @object = obj
        };
        var json = JsonConvert.SerializeObject(data);
        StartCoroutine(XapiPostLogin(json));
    }
    IEnumerator PostLogin(LoginModel mod)
    {
        WWWForm form = new WWWForm();
        form.AddField("email", mod.email);
        form.AddField("password", mod.password);
        form.AddField("type", mod.type);
        form.AddField("domainId", mod.domainId);
        LoadXapiModel(mod);

        using (UnityWebRequest www = UnityWebRequest.Post("https://workspapi.azurewebsites.net/api/login", form))
        {
            yield return www.SendWebRequest();

            if (www.isNetworkError || www.isHttpError)
            {
                Debug.Log(www.error);
            }
            else
            {
                Debug.Log("Form upload complete!");
                panel.gameObject.SetActive(false);
                Debug.Log(www.downloadHandler.text);
                SceneManager.LoadScene("Main");
            }
        }
    }

    public void GetValue()
    {
        LoginModel login = new LoginModel()
        {
            email = UserNameField.text.ToString(),
            password = PasswordField.text.ToString(),
            type = Usertype_Dropdown.captionText.text,
            domainId = m_Dropdown.itemText.text
        };
        StartCoroutine(PostLogin(login));
    }

    public string getname()
    {
        return UserNameField.text;
    }
    public void Dashboard_Screen()
    {
        if (true)
        {
            LoginPage.gameObject.SetActive(false);
        }
        LoginPage.gameObject.SetActive(false);
    }

}
public class LoginModel
{
    public string email { get; set; }
    public string password { get; set; }
    public string type { get; set; }
    public string domainId { get; set; }
}
