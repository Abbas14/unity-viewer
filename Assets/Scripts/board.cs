﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Video;
using Crosstales.FB;

public class board : MonoBehaviour
{
    string videoName;
    string path;
    public RawImage video;
    public VideoPlayer vdoplayer;
    public Plane pln;

    public void Pause()
    {
        if (vdoplayer.isPlaying == true)
        {
            vdoplayer.Pause();
        }
        else
        {
            vdoplayer.Play();
        }
    }

    public void BrowseVideo()
    {
        path = FileBrowser.OpenSingleFile("overwrite with png","", "*");
        if (path != null)
        {
            vdoplayer.url = path;
            vdoplayer.isLooping = true;
            vdoplayer.renderMode = UnityEngine.Video.VideoRenderMode.MaterialOverride;
            vdoplayer.targetMaterialRenderer = GetComponent<Renderer>();
            vdoplayer.targetMaterialProperty = "_MainTex";
            VideoClip clip = Resources.Load<VideoClip>(videoName);
            vdoplayer.clip = clip;
            vdoplayer.Play();
            
        }
    }
}
