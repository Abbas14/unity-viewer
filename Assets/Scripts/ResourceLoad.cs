﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ResourceLoad : MonoBehaviour {

    public void PDFboard()
    {
        GameObject myItem = Instantiate(Resources.Load("PdfBoard")) as GameObject;
        GameObject camera = GameObject.FindWithTag("MainCamera");
    }

    public void Wordboard()
    {
        GameObject myItem = Instantiate(Resources.Load("WordBoard")) as GameObject;
        GameObject camera = GameObject.FindWithTag("MainCamera");
    }

    public void Storageboard()
    {
        GameObject myItem = Instantiate(Resources.Load("StorageBoard")) as GameObject;
        StorageManagement script = gameObject.AddComponent<StorageManagement>() as StorageManagement;

        GameObject camera = GameObject.FindWithTag("MainCamera");
    }

    public void Streamboard()
    {
        GameObject myItem = Instantiate(Resources.Load("StreamBoard")) as GameObject;
        GameObject camera = GameObject.FindWithTag("MainCamera");
    }

    public void Whiteboard()
    {
        GameObject myItem = Instantiate(Resources.Load("WhiteBoard")) as GameObject;
        GameObject camera = GameObject.FindWithTag("MainCamera");
    }

    public void Databoard()
    {
        GameObject myItem = Instantiate(Resources.Load("DataBoard")) as GameObject;
        GameObject camera = GameObject.FindWithTag("MainCamera");
    }
    public void VideoBaord()
    {
        GameObject myItem = Instantiate(Resources.Load("VideoBoard")) as GameObject;
        GameObject camera = GameObject.FindWithTag("MainCamera");
    }
}
