﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Networking;
using UnityEngine.SceneManagement;


public class Login : MonoBehaviour

{
    public InputField userNameField;
    public InputField passwordField;
    public Button loginButton;

    void Start()
    {
        StartCoroutine(PostAPI());
    }
    
    IEnumerator PostAPI()
    {
        WWWForm request = new WWWForm();
        request.AddField("myField", "myData");

        using (UnityWebRequest www = UnityWebRequest.Post("http://192.168.1.6:3300/api/login", request))
        {
            yield return www.SendWebRequest();

            if (www.isNetworkError || www.isHttpError)
            {
                Debug.Log(www.error);
            }
            else
            {
                Debug.Log("Form upload complete!");
            }
        }
    }
}
