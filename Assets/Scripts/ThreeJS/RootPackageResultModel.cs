﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace Assets.Scripts.ThreeJS
{
    class RootPackageResultModel
    {
        public List<Items> Items { get; set; }
    }
    public class Items
    {
        public int APP_ID { get; set; }
        public string APPLICATION_NAME { get; set; }
        public string APPLICATION_VERSION { get; set; }
        public object APPLICATION_SERVICE_PACK { get; set; }
        public string APPLICATION_PATCH_LEVEL { get; set; }
    }
}
