﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace Assets.Scripts.ThreeJS
{
    public class PackageModel
    {
        public string FileName { get; set; }
        public List<AzureFunction> AzureFunction { get; set; }
    }
    public class Param
    {
        public string username { get; set; }
        public string packname { get; set; }
        public string queryname { get; set; }
    }

    public class AzureFunction
    {
        public string AuthType { get; set; }
        public string RequestURL { get; set; }
        public string FunctionKey { get; set; }
        public string FunctionName { get; set; }
        public List<Param> Params { get; set; }
    }

}
