using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class DrawingController: MonoBehaviour {
	// drawing canvas resolution
	const int TEX_WIDTH = 2000;
	const int TEX_HEIGHT = 2000;
	
	// Main camera, use to ray cast from cameras
	public Camera MainCamera;
	// drawing plan, aim texture to modified
	public GameObject targetPlane;
	
	// brush property
	public int penSize;
	public int eraserSize;
	int brushSize;
	public Color color;

	private Texture2D bg;

	private Vector2 curPosition = Vector2.zero;
	private Vector2 lastPosition = Vector2.zero;
	
	void Start () {

		bg = (Texture2D)GetComponent<Renderer>().material.mainTexture;
		ClearPanel();
	}


	void Update () 
	{
		RaycastHit hit;
		Ray ray= MainCamera.ScreenPointToRay(Input.mousePosition);
		if (!Physics.Raycast (ray, out hit))			
			return;
		if(hit.transform.name==targetPlane.transform.name)
		{

			if(Input.GetMouseButton(0))
			{
				Vector2 pixelUV = hit.textureCoord;
				pixelUV.x *= bg.width;
				pixelUV.y *= bg.height;
				curPosition = pixelUV;
				if(lastPosition != Vector2.zero)
				{
					Vector2[] pixelArr = InterpolatePositions(lastPosition,curPosition);				
					for(int i = 0 ; i < pixelArr.Length ; i++)
					{
						if(color == Color.clear)
						{
							brushSize = eraserSize;
							drawCircle(bg,(int)pixelArr[i].x, (int)pixelArr[i].y, brushSize);
						}
						else
						{
							Brush(bg,(int)pixelArr[i].x, (int)pixelArr[i].y,color);
						}

					}
					bg.Apply(true);
				}
				lastPosition = pixelUV;
			}
			else
			{
				lastPosition = Vector2.zero;
			}
		}
	}


	Vector2[] InterpolatePositions(Vector2 startPosition, Vector2 endPosition)
	{
		float units = Vector2.Distance(startPosition,endPosition);

		if(units == 0)
			units = 1;
		Vector2[] vec2Arr = new Vector2[(int)units+1];
		float counter = 0.0f;

		while(counter <= units)
		{
			vec2Arr[(int)counter] = Vector2.Lerp(startPosition, endPosition, counter/units);
			counter+=1.0f;
		}
		return vec2Arr;
	}

	private void drawCircle( Texture2D tex, int centerX,  int centerY,  int radius) {
		int d = 3 - (2 * radius);
		int x = 0;
		int y = radius;
		
		do {
			tex.SetPixel(centerX + x, centerY + y, color);
			tex.SetPixel(centerX + x, centerY - y, color);
			tex.SetPixel(centerX - x, centerY + y, color);
			tex.SetPixel(centerX - x, centerY - y, color);
			tex.SetPixel(centerX + y, centerY + x, color);
			tex.SetPixel(centerX + y, centerY - x, color);
			tex.SetPixel(centerX - y, centerY + x, color);
			tex.SetPixel(centerX - y, centerY - x, color);
			if (d < 0) {
				d = d + (4 * x) + 6;
			} else {
				d = d + 4 * (x - y) + 10;
				y--;
			}
			x++;
		} while (x <= y);
	}

	/// <summary>
	/// this function creates brush strokes.
	/// </summary>
	/// <param name="tex">Tex.</param>
	/// <param name="cx">Cx.</param>
	/// <param name="cy">Cy.</param>
	/// <param name="col">Col.</param>
	public void Brush(Texture2D tex, int cx, int cy, Color col)
	{

		SetPixel(tex,cx, cy, new Color(col.r,col.g,col.b,1));
		SetPixel(tex,cx-1, cy, new Color(col.r,col.g,col.b,1));
		SetPixel(tex,cx-2, cy, new Color(col.r,col.g,col.b,0.8f));
		SetPixel(tex,cx-3, cy, new Color(col.r,col.g,col.b,0.5f));
		SetPixel(tex,cx-4, cy, new Color(col.r,col.g,col.b,0.2f));
		
		SetPixel(tex,cx, cy-1, new Color(col.r,col.g,col.b,1));
		SetPixel(tex,cx, cy-2, new Color(col.r,col.g,col.b,0.8f));
		SetPixel(tex,cx, cy-3, new Color(col.r,col.g,col.b,0.5f));
		SetPixel(tex,cx, cy-4, new Color(col.r,col.g,col.b,0.2f));
		
		SetPixel(tex,cx-1, cy-1, new Color(col.r,col.g,col.b,0.9f));
		SetPixel(tex,cx-2, cy-1, new Color(col.r,col.g,col.b,0.7f));
		SetPixel(tex,cx-3, cy-1, new Color(col.r,col.g,col.b,0.4f));
		SetPixel(tex,cx-4, cy-1, new Color(col.r,col.g,col.b,0.1f));
		
		SetPixel(tex,cx-1, cy-1, new Color(col.r,col.g,col.b,0.9f));
		SetPixel(tex,cx-1, cy-2, new Color(col.r,col.g,col.b,0.7f));
		SetPixel(tex,cx-1, cy-3, new Color(col.r,col.g,col.b,0.4f));
		SetPixel(tex,cx-1, cy-4, new Color(col.r,col.g,col.b,0.1f));
		
		SetPixel(tex,cx-2, cy-2, new Color(col.r,col.g,col.b,0.6f));
		SetPixel(tex,cx-3, cy-2, new Color(col.r,col.g,col.b,0.3f));
		
		SetPixel(tex,cx-2, cy-2, new Color(col.r,col.g,col.b,0.6f));
		SetPixel(tex,cx-2, cy-3, new Color(col.r,col.g,col.b,0.3f));
		
		
		
		cx+=1;
		SetPixel(tex,cx, cy, new Color(col.r,col.g,col.b,1));
		SetPixel(tex,cx+1, cy, new Color(col.r,col.g,col.b,1));
		SetPixel(tex,cx+2, cy, new Color(col.r,col.g,col.b,0.8f));
		SetPixel(tex,cx+3, cy, new Color(col.r,col.g,col.b,0.5f));
		SetPixel(tex,cx+4, cy, new Color(col.r,col.g,col.b,0.2f));
		
		SetPixel(tex,cx, cy-1, new Color(col.r,col.g,col.b,1));
		SetPixel(tex,cx, cy-2, new Color(col.r,col.g,col.b,0.8f));
		SetPixel(tex,cx, cy-3, new Color(col.r,col.g,col.b,0.5f));
		SetPixel(tex,cx, cy-4, new Color(col.r,col.g,col.b,0.2f));
		
		SetPixel(tex,cx+1, cy-1, new Color(col.r,col.g,col.b,0.9f));
		SetPixel(tex,cx+2, cy-1, new Color(col.r,col.g,col.b,0.7f));
		SetPixel(tex,cx+3, cy-1, new Color(col.r,col.g,col.b,0.4f));
		SetPixel(tex,cx+4, cy-1, new Color(col.r,col.g,col.b,0.1f));
		
		SetPixel(tex,cx+1, cy-1, new Color(col.r,col.g,col.b,0.9f));
		SetPixel(tex,cx+1, cy-2, new Color(col.r,col.g,col.b,0.7f));
		SetPixel(tex,cx+1, cy-3, new Color(col.r,col.g,col.b,0.4f));
		SetPixel(tex,cx+1, cy-4, new Color(col.r,col.g,col.b,0.1f));
		
		SetPixel(tex,cx+2, cy-2, new Color(col.r,col.g,col.b,0.6f));
		SetPixel(tex,cx+3, cy-2, new Color(col.r,col.g,col.b,0.3f));
		
		SetPixel(tex,cx+2, cy-2, new Color(col.r,col.g,col.b,0.6f));
		SetPixel(tex,cx+2, cy-3, new Color(col.r,col.g,col.b,0.3f));
		
		
		
		
		cy+=1;
		SetPixel(tex,cx, cy, new Color(col.r,col.g,col.b,1));
		SetPixel(tex,cx+1, cy, new Color(col.r,col.g,col.b,1));
		SetPixel(tex,cx+2, cy, new Color(col.r,col.g,col.b,0.8f));
		SetPixel(tex,cx+3, cy, new Color(col.r,col.g,col.b,0.5f));
		SetPixel(tex,cx+4, cy, new Color(col.r,col.g,col.b,0.2f));
		
		SetPixel(tex,cx, cy+1, new Color(col.r,col.g,col.b,1));
		SetPixel(tex,cx, cy+2, new Color(col.r,col.g,col.b,0.8f));
		SetPixel(tex,cx, cy+3, new Color(col.r,col.g,col.b,0.5f));
		SetPixel(tex,cx, cy+4, new Color(col.r,col.g,col.b,0.2f));
		
		SetPixel(tex,cx+1, cy+1, new Color(col.r,col.g,col.b,0.9f));
		SetPixel(tex,cx+2, cy+1, new Color(col.r,col.g,col.b,0.7f));
		SetPixel(tex,cx+3, cy+1, new Color(col.r,col.g,col.b,0.4f));
		SetPixel(tex,cx+4, cy+1, new Color(col.r,col.g,col.b,0.1f));
		
		SetPixel(tex,cx+1, cy+1, new Color(col.r,col.g,col.b,0.9f));
		SetPixel(tex,cx+1, cy+2, new Color(col.r,col.g,col.b,0.7f));
		SetPixel(tex,cx+1, cy+3, new Color(col.r,col.g,col.b,0.4f));
		SetPixel(tex,cx+1, cy+4, new Color(col.r,col.g,col.b,0.1f));
		
		SetPixel(tex,cx+2, cy+2, new Color(col.r,col.g,col.b,0.6f));
		SetPixel(tex,cx+3, cy+2, new Color(col.r,col.g,col.b,0.3f));
		
		SetPixel(tex,cx+2, cy+2, new Color(col.r,col.g,col.b,0.6f));
		SetPixel(tex,cx+2, cy+3, new Color(col.r,col.g,col.b,0.3f));
		
		
		
		cx-=1;
		SetPixel(tex,cx, cy, new Color(col.r,col.g,col.b,1));
		SetPixel(tex,cx-1, cy, new Color(col.r,col.g,col.b,1));
		SetPixel(tex,cx-2, cy, new Color(col.r,col.g,col.b,0.8f));
		SetPixel(tex,cx-3, cy, new Color(col.r,col.g,col.b,0.5f));
		SetPixel(tex,cx-4, cy, new Color(col.r,col.g,col.b,0.2f));
		
		SetPixel(tex,cx, cy+1, new Color(col.r,col.g,col.b,1));
		SetPixel(tex,cx, cy+2, new Color(col.r,col.g,col.b,0.8f));
		SetPixel(tex,cx, cy+3, new Color(col.r,col.g,col.b,0.5f));
		SetPixel(tex,cx, cy+4, new Color(col.r,col.g,col.b,0.2f));
		
		SetPixel(tex,cx-1, cy+1, new Color(col.r,col.g,col.b,0.9f));
		SetPixel(tex,cx-2, cy+1, new Color(col.r,col.g,col.b,0.7f));
		SetPixel(tex,cx-3, cy+1, new Color(col.r,col.g,col.b,0.4f));
		SetPixel(tex,cx-4, cy+1, new Color(col.r,col.g,col.b,0.1f));
		
		SetPixel(tex,cx-1, cy+1, new Color(col.r,col.g,col.b,0.9f));
		SetPixel(tex,cx-1, cy+2, new Color(col.r,col.g,col.b,0.7f));
		SetPixel(tex,cx-1, cy+3, new Color(col.r,col.g,col.b,0.4f));
		SetPixel(tex,cx-1, cy+4, new Color(col.r,col.g,col.b,0.1f));
		
		SetPixel(tex,cx-2, cy+2, new Color(col.r,col.g,col.b,0.6f));
		SetPixel(tex,cx-3, cy+2, new Color(col.r,col.g,col.b,0.3f));
		
		SetPixel(tex,cx-2, cy+2, new Color(col.r,col.g,col.b,0.6f));
		SetPixel(tex,cx-2, cy+3, new Color(col.r,col.g,col.b,0.3f));

	}

	/// <summary>
	/// Sets the pixel. with respect to alpha.
	/// </summary>
	/// <param name="tex">Tex.</param>
	/// <param name="cx">Cx.</param>
	/// <param name="cy">Cy.</param>
	/// <param name="col">Col.</param>
	private void SetPixel(Texture2D tex, int cx, int cy, Color col){
		Color c = tex.GetPixel(cx,cy);

		float r = Mathf.Abs(c.r - col.r);
		float g = Mathf.Abs(c.g - col.g);
		float b = Mathf.Abs(c.b - col.b);

		r*=255;
		g*=255;
		b*=255;

		if(r < 1 && g < 1 && b < 1){

			if(c.a < col.a)
				tex.SetPixel(cx, cy, new Color(col.r,col.g,col.b,col.a));
			else
				tex.SetPixel(cx, cy, new Color(col.r,col.g,col.b,c.a));
		}
		else{
			tex.SetPixel(cx, cy, new Color(col.r,col.g,col.b,col.a));
		}
	}



	public void SetBrushColor(Color c){
		color = c;
	}

	public void SetEraser(){
		color = Color.clear;
	}

	public void ClearPanel()
	{
		Color[] colorArr = new Color[TEX_WIDTH*TEX_HEIGHT];
		for (int i = 0; i<TEX_WIDTH*TEX_HEIGHT; i++)
		{
			colorArr[i] = Color.clear;
		}
		bg.SetPixels(colorArr);
		bg.Apply(false);
	}
	
	void OnDestroy() {
       	ClearPanel();
    }
}


