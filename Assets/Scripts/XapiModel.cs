﻿public class XapiModel
{

    public class Actor
    {
        public string mbox { get; set; }
    }

    public class Display
    {
        public string Username { get; set; }
        public string Domain { get; set; }
        public string Type { get; set; }
    }

    public class Verb
    {
        public Display display { get; set; }
        public string id { get; set; }
    }


    public class Object
    {
        public string id { get; set; }
    }

    public class DataModel
    {
        public Actor actor { get; set; }
        public Verb verb { get; set; }
        public Object @object { get; set; }
    }
}
