﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Crosstales.FB;
//using GracesGames.SimpleFileBrowser.Scripts;

public class Assetloader : MonoBehaviour {

    public Button AssetLoader;

    // Use this for initialization
    void Start () {
        AssetLoader.onClick.AddListener(() => { assetloader(); });
    }
	
	// Update is called once per frame
	void Update () {



        }

    public void assetloader()
    {
        //string path = FileBrowser.OpenSingleFile("Selected File", "", "*");
        string path = FileBrowser.OpenSingleFile("Selected File", "", ".*");
        LoadAsset(path);
    }

    private void LoadAsset(string assetBundleName)
    {
        string filePath = System.IO.Path.Combine(Application.streamingAssetsPath, "AssetBundles");
        filePath = System.IO.Path.Combine(filePath, assetBundleName);

        //Load "animals" AssetBundle
        var assetBundleCreateRequest = AssetBundle.LoadFromFileAsync(filePath);
        //yield return assetBundleCreateRequest;

        AssetBundle asseBundle = assetBundleCreateRequest.assetBundle;
        string[] listAssest = asseBundle.AllAssetNames();
        for (int i = 0; i < listAssest.Length; i++)
        {
            AssetBundleRequest asset = asseBundle.LoadAssetAsync<GameObject>(listAssest[i]);
            GameObject loadedAsset = asset.asset as GameObject;
            Instantiate(loadedAsset);
        }

        
        ////Load the "dog" Asset (Use Texture2D since it's a Texture. Use GameObject if prefab)
        //AssetBundleRequest asset = asseBundle.LoadAssetAsync<Texture2D>(objectNameToLoad);
        //yield return asset;

        ////Retrieve the object (Use Texture2D since it's a Texture. Use GameObject if prefab)
        //Texture2D loadedAsset = asset.asset as Texture2D;

        ////Do something with the loaded loadedAsset  object (Load to RawImage for example) 
        //image.texture = loadedAsset;
    }
}
