﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.IO;
using UnityEngine.SceneManagement;

public class CreateNewEvents : MonoBehaviour
{

	public void CreateFuntion()
    {
        SceneManager.LoadScene("Main");
    }
    public void UserMode_Scene()
    {
        SceneManager.LoadScene("UserMode");
    }
    public void RoomPort()
    {
        SceneManager.LoadScene("VR Room");
    }
    public void SecondScene()
    {
        SceneManager.LoadScene("SecondScene");
    }
}
