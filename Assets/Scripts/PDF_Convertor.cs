﻿using Paroxe.PdfRenderer;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEditor;
using UnityEngine;
using UnityEngine.UI;
using System.Runtime.InteropServices;
using System.Diagnostics;
using Crosstales.FB;

public class PDF_Convertor : MonoBehaviour
{
    public static string tempfileName { get; set; }
    public string ConvertFiles;
    public static void GetTempPath()
    {
        string result = Path.GetTempPath();
    }

    public void ConvertFile()
    {
        string path = FileBrowser.OpenSingleFile("Selected File", "", ".docx");
        string pathdocx = path;
        path = path.Replace(".docx", ".pdf");
        string filename = Path.GetFileName(path);
        string path1 = Application.dataPath + "/temp/" + filename;
        tempfileName = path1;

#if UNITY_EDITOR_64
        string PluginServerPath = Application.dataPath + @"\WordToPdf\PluginServer\x64";
#else
#if UNITY_EDITOR_32
        string PluginServerPath = Application.dataPath + @"\WordToPdf\PluginServer\x86";
#else
        //HACK
        string AssemblyPath=System.Reflection.Assembly.GetExecutingAssembly().Location;
        //log this for error handling
        //Debug.Log("Assembly path:"+AssemblyPath);

        AssemblyPath = Path.GetDirectoryName(AssemblyPath); //Managed
      
        AssemblyPath = Directory.GetParent(AssemblyPath).FullName; //<project>_Data
        AssemblyPath = Directory.GetParent(AssemblyPath).FullName;//required

        string PluginServerPath=AssemblyPath+@"\PluginServer";
#endif
#endif
        string args = pathdocx + " " + path1;
        try
        {
            Process process = new Process();
            var _pluginProcess = new System.Diagnostics.Process()
            {
                StartInfo = new System.Diagnostics.ProcessStartInfo()
                {
                    WorkingDirectory = PluginServerPath,
                    FileName = PluginServerPath + @"\WordToPdf.exe",
                    Arguments = args,
                    CreateNoWindow = true,
                    UseShellExecute = false
                }
            };
            _pluginProcess.Start();
        }
        catch (Exception)
        {
            throw;
        }
    }
}
