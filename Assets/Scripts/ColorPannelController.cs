﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class ColorPannelController : MonoBehaviour {

	public Camera MainCamera;
	public DrawingController drawingController;

	public SpriteRenderer customColor;

	public Slider r_color;
	public Slider g_color;
	public Slider b_color;


	void Update () 
	{
		if(Input.GetMouseButtonDown(0))
		{

			Vector3 mousePos = Input.mousePosition;
			mousePos.z = 10;
			
			Vector3 screenPos = MainCamera.ScreenToWorldPoint(mousePos);
			
			RaycastHit2D hit = Physics2D.Raycast(screenPos,Vector2.zero);
			
			if(hit)
			{
				if(hit.transform.name.Contains("BrushColor"))
					SelectBrushColor(hit.transform);
			}
		}

		customColor.color = new Color(r_color.value,g_color.value,b_color.value,1.0f);

	}

	private void SelectBrushColor(Transform t){

		drawingController.SetBrushColor(t.GetComponent<SpriteRenderer>().color);

	}

}
