﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[Serializable]
internal class DropBoxItems
{
    public entries[] entries;
}


[Serializable]
internal class entries
{
    public string id;
    public string path;
    public string name;
    public string path_lower;
    public string path_display;
    public string tag;


}