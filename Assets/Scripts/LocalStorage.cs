﻿using Assets.Scripts.Model;
using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using TriLib;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class LocalStorage : MonoBehaviour
{
    public Button LocalStorage_btn;
    public GameObject LocalFileList;
    public Text LocalFileName;
    // Use this for initialization
    void Start()
    {
        LocalStorage_btn.onClick.AddListener(() => { StorageAssetBtn(); });
    }
    public void StorageAssetBtn()
    {
        var dir = Directory.GetDirectories(ApplicationContext.persDataPath);
        foreach (var item in dir)
        {
            DirectoryInfo dirinfo = new DirectoryInfo(item);
            var folderName = dirinfo.Name;

            LocalFileName.text = folderName;
            GameObject dorpboxobj = Instantiate(LocalFileList);
            dorpboxobj.transform.SetParent(transform, false);
            dorpboxobj.AddComponent<EventTrigger>();
            EventTrigger trigger = dorpboxobj.GetComponent<EventTrigger>();
            EventTrigger.Entry entry = new EventTrigger.Entry
            {
                eventID = EventTriggerType.PointerClick
            };
            entry.callback.AddListener((eventData) =>
            {
                LoadFile(eventData);
            });
            trigger.triggers.Add(entry);
        }
    }

    public void LoadFile(BaseEventData baseEvent)
    {
        PointerEventData pointerEventData = (PointerEventData)baseEvent;
        GameObject gameObject = pointerEventData.pointerCurrentRaycast.gameObject;
        var text = gameObject.GetComponent<Text>().text;
        if (string.IsNullOrEmpty(text))
            return;
        var files = Directory.GetFiles(ApplicationContext.persDataPath + "/" + text);
        foreach (var fls in files)
        {
            if(fls.ToLower().Contains("json"))
            {
                
                CovertModelJson(fls);
            }
            else
            {
                LoadGameObject(fls);
            }

        }
        


    }
    public void LoadGameObject(string path)
    {
        using (var assetLoader = new AssetLoader())
        {
            try
            {
                var assetLoaderOptions = AssetLoaderOptions.CreateInstance();
                assetLoaderOptions.RotationAngles = new Vector3(90, 0, 0);
                assetLoaderOptions.AutoPlayAnimations = true;
                var objes = assetLoader.LoadFromFile(path, assetLoaderOptions);
            }
            catch (Exception ex)
            {
                Debug.Log(ex.Message);
            }
        }

    }
    private void CovertModelJson(string path)
    {
        var data = File.ReadAllText(path);
        var rootObjectModels = JsonConvert.DeserializeObject<List<RootObjectModel>>(data);
        if (ApplicationContext.PackageRootObjectModel == null)
        {
            ApplicationContext.PackageRootObjectModel = new List<RootObjectModel>();
        }
        ApplicationContext.PackageRootObjectModel.AddRange(rootObjectModels);

    }
}
