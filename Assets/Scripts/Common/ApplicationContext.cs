﻿using Assets.Scripts.Model;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public static class ApplicationContext
{
    public static string Token
    {
        get { return "DIQsDNtIphAAAAAAAAABpshw9d9gZhKJfm4sMr8GGsu_69h1qSKQhYVUZM-STEOF"; }
    }

    public static string DropboxlocalPath
    {
        get
        {
            return Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + "\\DropBox\\";

        }
    }

    public static string APIDropBoxDowanldURI
    {
        get { return "https://content.dropboxapi.com/2/files/download"; }
    }

    public static string APIDropBoxGetFileURI
    {
        get
        {
            return "https://api.dropboxapi.com/2/files/list_folder";
        }
    }

    public static bool ISGizmo;

  
    public static GameObject SelectedGameObject;

    public static List<RootObjectModel> PackageRootObjectModel;


    public static string persDataPath
    {
        get
        {
            return Path.Combine(Application.persistentDataPath, "data");
        }

    }


}