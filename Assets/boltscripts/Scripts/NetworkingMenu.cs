﻿using UnityEngine;
using System.Collections;
using System;
using UdpKit;
using UnityEngine.UI;

public class NetworkingMenu : Bolt.GlobalEventListener
{
    public Text IdField;
    public  void host()
    {
        BoltLauncher.StartServer();
    }
    public void Joint()
    {
        GuiId_Genrate IdField = GetComponent<GuiId_Genrate>();
        GuiId_Genrate id = gameObject.GetComponent<GuiId_Genrate>();
     var dpMa =  id.GetComponent<InputField>();
        if(IdField.Value.text == dpMa.text)
         {
           BoltLauncher.StartClient();
        }
    }
   
    public override void BoltStartDone()
    {
        if (BoltNetwork.IsServer)
        {
            string matchName = Guid.NewGuid().ToString();
            BoltNetwork.SetServerInfo(matchName, null);
            BoltNetwork.LoadScene("VR Room");
        }
    }

    public override void SessionListUpdated(Map<Guid, UdpSession> sessionList)
    {
        Debug.LogFormat("Session list updated: {0} total sessions", sessionList.Count);

        foreach (var session in sessionList)
        {
            UdpSession photonSession = session.Value as UdpSession;

            if (photonSession.Source == UdpSessionSource.Photon)
            {
                BoltNetwork.Connect(photonSession);
            }
        }
    }
  
}