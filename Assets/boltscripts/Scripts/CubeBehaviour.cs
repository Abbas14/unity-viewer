﻿using UnityEngine;
using System.Collections;

public class CubeBehaviour : Bolt.EntityBehaviour<IVRroomState>
{
    public GameObject[] WeaponObjects;
    public override void Attached()
    {
        state.SetTransforms(state.VRroomStateTransfrom, transform);
        if (entity.isAttached)
        {

            //// NEW: On the owner, we want to setup the weapons, the Id is set just as the index and the Ammo is randomized between 50 to 100
            for (int i = 0; i < state.boards.Length; i++)
            {
                state.boards[i].VRroomid = Random.Range(0, WeaponObjects.Length - 1);
                state.boards[i].VRroomammo = Random.Range(50, 100);
            }

            //NEW: by default we don't have any weapon up, so set index to -1
            state.boardsIndex = 1;
           
        }
        state.AddCallback("boardsIndex", WeaponActiveIndexChanged);
    }
    void WeaponActiveIndexChanged()
    {
        for (int i = 0; i < WeaponObjects.Length; i++)
        {
            WeaponObjects[i].SetActive(false);
        }
        
        if (state.boardsIndex >= 0)
        {
            int objectId = state.boards[state.boardsIndex].VRroomid;
            WeaponObjects[objectId].SetActive(true);
        }
    }
    public override void SimulateOwner()
    {
        var speed = 4f;
        var movement = Vector3.zero;

        if (Input.GetKey(KeyCode.W)) { movement.z += 1; }
        if (Input.GetKey(KeyCode.S)) { movement.z -= 1; }
        if (Input.GetKey(KeyCode.A)) { movement.x -= 1; }
        if (Input.GetKey(KeyCode.D)) { movement.x += 1; }
        if (Input.GetKeyDown(KeyCode.Alpha1)) state.boardsIndex = 0;
        if (Input.GetKeyDown(KeyCode.Alpha2)) state.boardsIndex = 1;
        if (Input.GetKeyDown(KeyCode.Alpha3)) state.boardsIndex = 2;
        if (Input.GetKeyDown(KeyCode.Alpha0)) state.boardsIndex = -1;

        if (movement != Vector3.zero)
        {
            transform.position = transform.position + (movement.normalized * speed * BoltNetwork.FrameDeltaTime);
        }
    }
   
}